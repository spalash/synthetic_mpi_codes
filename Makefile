#Compiler for compiling programs: mpicc or mpic++
CC = mpicc 
#CC = CC

CFLAGS  = -g -Wall -O3

all : benchmark_1.c benchmark_2.c
	$(CC) $(CFLAGS) -o benchmark_1 benchmark_1.c
	$(CC) $(CFLAGS) -o benchmark_2 benchmark_2.c
	$(CC) $(CFLAGS) -o benchmark_3 benchmark_3.c
	$(CC) $(CFLAGS) -o random randMPI.c	

bm1 : benchmark_1.c
	$(CC) $(CFLAGS) -o benchmark_1 benchmark_1.c	

bm2 : benchmark_2.c
	$(CC) $(CFLAGS) -o benchmark_2 benchmark_2.c

bm3 : benchmark_3.c
	$(CC) $(CFLAGS) -o benchmark_3 benchmark_3.c

random : randMPI.c
	$(CC) $(CFLAGS) -o random randMPI.c	

clean :
	rm -f benchmark_1 benchmark_2 benchmark_3 random
