#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <limits.h>
#include <time.h>
#include "mpi.h"

int main(int argc, char** argv){
	MPI_Init(&argc, &argv);
	
	unsigned  seed;
	int i, j, k,msg_size, rank, size, new_neigh, num_neighbors, *is_rank_selected=NULL, *neighbors=NULL, chunks, total_iter, *rank_neighbors=NULL, *num_rank_neighbors=NULL, max_neigh_iter,  neighbors_per_chunk=0;
	long num_send = 0, chunks_done=0;
	char *recv_msg_buf=NULL, *send_msg_buf=NULL;
	double programStartTime=0, comp_time, start_of_comp_time, per_chunk_comp_time;
	MPI_Request *srequest = NULL, *rrequest = NULL;
	MPI_Status *sstatus = NULL;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	msg_size = 1024;
	num_neighbors = size/1000+1;
	chunks = 1;
	comp_time = 1;
	total_iter = 1;

	for(i=0;i<argc;i++){
		if(!strcmp(argv[i],"-s")){
			msg_size=atoi(argv[i+1]);
		}
		if(!strcmp(argv[i],"-c")){
			chunks=atoi(argv[i+1]);
		}
		if(!strcmp(argv[i],"-n")){
			num_neighbors=atof(argv[i+1]);
		}
		if(!strcmp(argv[i],"-w")){
			comp_time=atof(argv[i+1]);
		}
		if(!strcmp(argv[i],"-i")){
			total_iter=atoi(argv[i+1]);
		}
	}


	seed = (unsigned)time(NULL);

	MPI_Bcast( &seed, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD );

	srand(seed);

	rank_neighbors = (int*)malloc(num_neighbors*size*sizeof(int));
	num_rank_neighbors = (int*)calloc(size, sizeof(int));
	is_rank_selected = (int *)malloc(size*sizeof(int));

	max_neigh_iter = 10*num_neighbors;

	for(i = 0; i < size; i++){
		if(num_rank_neighbors[i] != num_neighbors){
			memset(is_rank_selected, 0, size*sizeof(int));
			j = 0;
			k = 0;
			is_rank_selected[i] = 1;

			while( j < num_neighbors && k < max_neigh_iter ){
				if(j < num_rank_neighbors[i]){
					is_rank_selected[rank_neighbors[i*num_neighbors+j]] = 1;
					j++;
					continue;
				}

				k++;
				
				new_neigh = rand() % size;

		  	  	if( is_rank_selected[new_neigh] || num_rank_neighbors[new_neigh] == num_neighbors )
		    		continue;

		    	is_rank_selected[new_neigh] = 1;
		  		rank_neighbors[i*num_neighbors+j] = new_neigh;
		  		num_rank_neighbors[i]++;

		  		rank_neighbors[new_neigh*num_neighbors + num_rank_neighbors[new_neigh]] = i;
		  		num_rank_neighbors[new_neigh]++;
		  	  	
		  	  	j++;
			}
		}
	}

	// if(!rank){
	// 	for(i = 0; i < size; i++){
	// 		printf("%d  has %d neighbors : ", i, num_rank_neighbors[i]);
	// 		for(j = 0; j < num_rank_neighbors[i]; j++ )
	// 			printf("%d ", rank_neighbors[i*num_neighbors + j]);
	// 		printf("\n");
	// 	}
	// }

	neighbors = (int*)malloc(num_rank_neighbors[rank]*sizeof(int));

	for( i = 0; i < num_rank_neighbors[rank]; i++ )
		neighbors[i] = rank_neighbors[rank * num_neighbors + i];

	num_neighbors = num_rank_neighbors[rank];

	free(num_rank_neighbors);
	free(rank_neighbors);
	free(is_rank_selected);

	send_msg_buf = (char *)malloc(msg_size*sizeof(char));
	recv_msg_buf = (char *)malloc(msg_size*sizeof(char));
	srequest = (MPI_Request*)malloc(num_neighbors*sizeof(MPI_Request));
	rrequest = (MPI_Request*)malloc(num_neighbors*sizeof(MPI_Request));
	sstatus = (MPI_Status*)malloc(num_neighbors*sizeof(MPI_Status));
	
	per_chunk_comp_time = comp_time/chunks;

	MPI_Barrier(MPI_COMM_WORLD);

	programStartTime = MPI_Wtime();

	if(chunks>num_neighbors)
		chunks=num_neighbors;

	neighbors_per_chunk = (num_neighbors-1)/chunks+1;
	
	for( i = 0; i < total_iter; i++){
		for(j = 0; j < num_neighbors; j++)
			MPI_Isend(send_msg_buf, msg_size, MPI_CHAR, neighbors[j], 0, MPI_COMM_WORLD, &srequest[j]);
		num_send+=j;

		j=0;
		while((chunks*neighbors_per_chunk-chunks+j)<num_neighbors){
			for(k = 0; k < neighbors_per_chunk; k++ )
				MPI_Irecv(recv_msg_buf, msg_size, MPI_CHAR, neighbors[j*neighbors_per_chunk+k], 0, MPI_COMM_WORLD, &rrequest[k]);
			MPI_Waitall(k, rrequest, sstatus);			
			start_of_comp_time = MPI_Wtime();
			while((MPI_Wtime()-start_of_comp_time) < per_chunk_comp_time);
			j++;
		}

		neighbors_per_chunk = neighbors_per_chunk - 1; 
		chunks_done = j;

		while(j<chunks){
			for(k = 0; k < neighbors_per_chunk; k++ )
				MPI_Irecv(recv_msg_buf, msg_size, MPI_CHAR, neighbors[j*neighbors_per_chunk+chunks_done+k], 0, MPI_COMM_WORLD, &rrequest[k]);
			MPI_Waitall(k, rrequest, sstatus);			
			start_of_comp_time = MPI_Wtime();
			while((MPI_Wtime()-start_of_comp_time) < per_chunk_comp_time);
			j++;
		}
		MPI_Waitall(num_neighbors, srequest, sstatus);

		neighbors_per_chunk = neighbors_per_chunk + 1;
	}

	MPI_Barrier(MPI_COMM_WORLD);

	if(!rank){
		printf("Number of messages send %ld, Amount of Data send by a node %ld, The execution time is %lf\n", num_send, (long)(num_send*msg_size), MPI_Wtime() - programStartTime);
	}
	
	MPI_Finalize();

}
