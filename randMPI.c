#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <limits.h>
#include <time.h>
#include "mpi.h"

int main(int argc, char** argv){
	MPI_Init(&argc, &argv);
	int i, j, msg_size, rank, size, random, num_neighbors, *is_selected=NULL, *neighbors=NULL;
	char *recv_msg_buf=NULL, *send_msg_buf=NULL;
	double programStartTime=0, exec_time, wait_time, start_of_wait_time=0;
	MPI_Request *srequest=NULL, *rrequest=NULL;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	msg_size = 1024;
	num_neighbors=size/1000+1;
	exec_time=10;
	wait_time=0;

	for(i=0;i<argc;i++){
		if(!strcmp(argv[i],"-s")){
			msg_size=atoi(argv[i+1]);
		}
		if(!strcmp(argv[i],"-t")){
			exec_time=atof(argv[i+1]);
		}
		if(!strcmp(argv[i],"-n")){
			num_neighbors=atof(argv[i+1]);
		}
		if(!strcmp(argv[i],"-w")){
			wait_time=atof(argv[i+1]);
		}
	}
		

	//if(!rank)
	//	printf("\nDetails of Even Communicator\nNumber of Processes : %d\nMessage Size : %d bytes \nExpected Execution Time : %lf s\n\n", size, msg_size, exec_time);

	send_msg_buf=(char *)malloc(msg_size*sizeof(char));
	recv_msg_buf=(char *)malloc(msg_size*sizeof(char));
	is_selected=(int *)calloc(size,sizeof(int));
	srequest=(MPI_Request*)malloc(num_neighbors*sizeof(MPI_Request));
	rrequest=(MPI_Request*)malloc((2*num_neighbors+10)*sizeof(MPI_Request));
	neighbors=(int*)malloc(num_neighbors*sizeof(int));

	MPI_Barrier(MPI_COMM_WORLD);
	
  srand(rank);
	
	i=0;

  if(rank){
  	while(i<num_neighbors){
  		  random = (rand()%(size-1)) + 1;
  	  	if(is_selected[random])
    			continue;
    		is_selected[random]=1;
  		  neighbors[i]=random;
  	  	i++;
    	}

  	for(i = 0; i < num_neighbors; i++) {	
		  MPI_Isend(send_msg_buf, msg_size, MPI_CHAR, neighbors[i], 0, MPI_COMM_WORLD, &srequest[i]);
	  }
	  for(i = 0; i < 2*num_neighbors+10; i++) { 
	  	MPI_Irecv(recv_msg_buf, msg_size, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &rrequest[i]);
  	} 
	  j=0;
  }

  programStartTime=MPI_Wtime();
	
  while((MPI_Wtime()-programStartTime)<exec_time){
		if(rank){
			for(i = 0; i < num_neighbors; i++) {
				int done = 0;
				MPI_Test(&srequest[i], &done, MPI_STATUS_IGNORE);
				if(done) {
					printf("Message sent\n");
					MPI_Isend(send_msg_buf, msg_size, MPI_CHAR, neighbors[j], 0, MPI_COMM_WORLD, &srequest[i]);
					j=(j+1)%num_neighbors;
				}
			}

			for(i = 0; i < 2*num_neighbors+10; i++) { 
		    	int done = 0;
		    	MPI_Test(&rrequest[i], &done, MPI_STATUS_IGNORE);
			    if(done) {
					MPI_Irecv(recv_msg_buf, msg_size, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &rrequest[i]);
			    }
		  	}

		  	start_of_wait_time = MPI_Wtime();
			while((MPI_Wtime()-start_of_wait_time) < wait_time);
		}
	}

	if(!rank){
		MPI_Abort(MPI_COMM_WORLD,0);
	}
	MPI_Finalize();
}
